#include "Point3D.h"


Point3D::Point3D(float x, float y, float z){
	coord[0] = x;
	coord[1] = y;
	coord[2] = z;
}

Point3D&
Point3D::operator=(const Point3D& rhs) {
	if (&rhs != this) {
		coord[0] = rhs.coord[0];
		coord[1] = rhs.coord[1];
		coord[2] = rhs.coord[2];
	}
	return *this;
}


float
dotProduct(Point3D* a, Point3D* b){
	return a->coord[0] * b->coord[0] +  a->coord[1] * b->coord[1] +
		 a->coord[2] * b->coord[2];
}

float
dotProduct(Point3D* a, float* b){
	return a->coord[0] * b[0] +  a->coord[1] * b[1] +
		 a->coord[2] * b[2];
}


void
crossProduct (float* a, float* b, float* result){
	result[0] = a[1] * b[2] - a[2] * b[1];
	result[1] = a[2] * b[0] - a[0] * b[2];
	result[2] = a[0] * b[1] - a[1] * b[0];
}


float
length(Point3D* a) {
	return sqrt(a->coord[0]*a->coord[0]+ a->coord[1]*a->coord[1]+ a->coord[2]*a->coord[2]);
}

void
normalise(Point3D* a) {
	float f = length(a);
	a->coord[0] = a->coord[0] / f;
	a->coord[1] = a->coord[1] / f;
	a->coord[2] = a->coord[2] / f;
}


void
sub(const float* a, const float* b, float* result) {
	result[0] = a[0] - b[0];
	result[1] = a[1] - b[1];
	result[2] = a[2] - b[2];
}


void add(const float* a, const float* b, float* result){
	result[0] = a[0] + b[0];
	result[1] = a[1] + b[1];
	result[2] = a[2] + b[2];
}

void
mult(float scalar, Point3D* a){
	a->coord[0] = a->coord[0] * scalar;
	a->coord[1] = a->coord[1] * scalar;
	a->coord[2] = a->coord[2] * scalar;
}


void
mult(float scalar, Point3D* in, Point3D* out){
	out->coord[0] = in->coord[0] * scalar;
	out->coord[1] = in->coord[1] * scalar;
	out->coord[2] = in->coord[2] * scalar;
}


std::ostream& operator<<(std::ostream& os , const Point3D & p) {
	os << p.coord[0] << " " << p.coord[1] << " " << p.coord[2] ;
       	return os;
}

Point3D&
Point3D::operator+=(const Point3D& p) {
	add(this->coord , p.coord, this->coord);
	return *this;
}

