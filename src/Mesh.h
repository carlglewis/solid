
#ifndef CL_MESH_H
#define CL_MESH_H

#include <GL/gl.h>
#include "Point3D.h"
#include <iostream>

struct Vertex {
	int vertIndex;
	int normIndex;

};

struct Face {
	int nVerts;
	Vertex* v;

};


class Mesh {
protected:
	Face *faces;
	int numFaces;
	Point3D* vertices;
	int numVertices;
	Point3D* normals;
	int numNorms;
	void newell(int face);

public:
	Mesh(){}
	Mesh(const Mesh&);
	virtual ~Mesh(){}
	Mesh& operator=(const Mesh&);
	
	virtual void draw() const;


};


class Plane : public Mesh {

public:
	Plane();
	Plane(const Plane&);
	virtual ~Plane();
	Plane& operator=(const Plane &);	
	void computeNormal();

	// returns ith vertex as array of 3 floats
	const float* operator[](int i) { return vertices[i]; }
	
	friend std::istream& operator>>(std::istream&, Plane&);
	friend std::ostream& operator<<(std::ostream&, const Plane&);		

private:
	void alloc();
	void copy(const Plane&);	
};

std::istream& operator>>(std::istream&, Plane& );
std::ostream& operator<<(std::ostream&, const Plane&);




class Cube : public Mesh {

public:
	Cube();
	virtual ~Cube();

};

class BuckyBall;

class Icosahedron : public Mesh {

public :
	Icosahedron();
	virtual ~Icosahedron();

	static const float tau = 0.618034;

	friend class BuckyBall;

};


class BuckyBall : public Mesh {
public:
	BuckyBall();
	virtual ~BuckyBall();
protected:
	Icosahedron* i;
	void makeVertices();
	void makeFaces();
	static const int vdata[];
	static const int pentData[];
	static const int hexData[];

};

class Sphere : public Mesh {

public:
	/* Resolution gives the number of steps to use in the subdivision */
	/* of the spherical surface.  It must be positive, even and > 4 . */
	/* higher numbers are better, but the number of vertices grows as O(n^2) */	
	Sphere(int resolution);
	virtual ~Sphere();	

protected:
	int		n;   				// local name for resolution
	int		numCircles;
	void	makeVertices();
	void	makeFaces();
	void	makeNormals();
	void 	makeCircle(float radius, float z, Point3D* result);
	int		totalVertices();	// a simple function of n
	int		totalFaces();		// ditto
};

/*
 * Just like a sphere except it has red stripes
 * so you can see it roll. reimplements draw() so it can do the colors 
 */
class StripedSphere : public Sphere {

	const int listIndex;
public:	
	StripedSphere(int resolution);
	virtual ~StripedSphere(){}
	
	virtual void draw() const;
	void drawCommands();
	

};

#endif



