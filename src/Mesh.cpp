

#include "Mesh.h"
#include <string.h>
#define SQRT2 1.414213562
#define SQRT8 2.828427125


Mesh::Mesh(const Mesh & m) {}

Mesh&
Mesh::operator=(const Mesh&) { return *this; }


void
Mesh::draw () const{  //Hill p298

	for(int i = 0; i < numFaces ;i++) {
		//newell(i);
		glBegin(GL_POLYGON);
		for(int j = 0; j < faces[i].nVerts; j++) {
			int iv  = faces[i].v[j].vertIndex;
			int in  = faces[i].v[j].normIndex;
			glNormal3f(normals[in].coord[0], normals[in].coord[1],
				normals[in].coord[2]);
			glVertex3f(vertices[iv].coord[0], vertices[iv].coord[1], vertices[iv].coord[2]);
		}
		glEnd();
	}

}

void
Mesh::newell(int f) {  //f is face number
	//cout << "newell" <<endl;
	// x component
	int j;
	normals[f].coord[0] = 0;
	for (int i  = 0;i < faces[f].nVerts;i++) {
		j = (i+1) % faces[f].nVerts;
		normals[f].coord[0] += ( ( vertices[faces[f].v[i].vertIndex].coord[1] -
				       vertices[faces[f].v[j].vertIndex].coord[1] ) *
				      ( vertices[faces[f].v[i].vertIndex].coord[2] +
				       vertices[faces[f].v[j].vertIndex].coord[2] ) );

	}

	// y component
	normals[f].coord[1] = 0;
	for (int i  = 0;i < faces[f].nVerts ;i++) {
		j = (i+1) % faces[f].nVerts;
		normals[f].coord[1] +=  (( vertices[faces[f].v[i].vertIndex].coord[2] -
			       vertices[faces[f].v[j].vertIndex].coord[2] ) *
			      ( vertices[faces[f].v[i].vertIndex].coord[0] +
			       vertices[faces[f].v[j].vertIndex].coord[0]  ) )  ;
	}


       // z component
       normals[f].coord[2] = 0;
       for (int i  = 0;i < faces[f].nVerts;i++) {
		j = (i+1) % faces[f].nVerts;
		normals[f].coord[2] +=  (( vertices[faces[f].v[i].vertIndex].coord[0] -
			vertices[faces[f].v[j].vertIndex].coord[0] ) *
			( vertices[faces[f].v[i].vertIndex].coord[1] +
			vertices[faces[f].v[j].vertIndex].coord[1]  ) ) ;
	}


	//normals[f].coord[1] = - normals[f].coord[1] ;
	normalise(&normals[f]);
}


Plane::Plane() {
		
	alloc();
	newell(0);
}

Plane::Plane(const Plane& p) {
	
	alloc();
	copy(p);
	
}

Plane&
Plane::operator=(const Plane& p) {
	
	if(&p != this) copy(p);
	return *this;
}

void
Plane::alloc() {
	
	faces = new Face[numFaces = 1];
	vertices  = new Point3D[numVertices = 4];
	normals = new Point3D[numNorms = 1];

	faces[0].nVerts = 4;
	faces[0].v = new Vertex[4];

	for(int i = 0; i < 4 ; ++i) {
		faces[0].v[i].vertIndex = i;
		faces[0].v[i].normIndex = 0;
	}
}


void
Plane::copy(const Plane & p) {

	
	memcpy(vertices, p.vertices, sizeof(Point3D)*4);
	normals[0] = p.normals[0];
}


Plane::~Plane() {
	
	delete [] faces[0].v;
	delete [] faces; 
	delete [] vertices;
       	delete [] normals ;
	
}

void Plane::computeNormal() {
	newell(0);
}


std::istream& 
operator>>(std::istream& is, Plane & p) {
	
	for(int i = 0; i < 4 ; ++i)
		is >> p.vertices[i];

	return is;
	
}


std::ostream& operator<<(std::ostream& os, const Plane& p) {
	
	os << "PLANE: "; 
	for(int i = 0; i < 4 ; ++i)
		os << p.vertices[i] << " ";
       	os << "norm: "	<< p.normals[0];

	return os;
	
}

Cube::Cube() {

	vertices = new Point3D[8];
	faces = new Face[6];
	numFaces = 6;   numNorms = 6;
	normals = new Point3D[6];

	vertices[0].coord[0] = 1.0; vertices[0].coord[1] = 1.0;  vertices[0].coord[2] = 1.0;
	vertices[1].coord[0] = 1.0; vertices[1].coord[1] = 1.0;  vertices[1].coord[2] = 0.0;
	vertices[2].coord[0] = 0.0; vertices[2].coord[1] = 1.0;  vertices[2].coord[2] = 0.0;
	vertices[3].coord[0] = 0.0; vertices[3].coord[1] = 1.0;  vertices[3].coord[2] = 1.0;
	vertices[4].coord[0] = 1.0; vertices[4].coord[1] = 0.0;  vertices[4].coord[2] = 1.0;
	vertices[5].coord[0] = 1.0; vertices[5].coord[1] = 0.0;  vertices[5].coord[2] = 0.0;
	vertices[6].coord[0] = 0.0; vertices[6].coord[1] = 0.0;  vertices[6].coord[2] = 0.0;
	vertices[7].coord[0] = 0.0; vertices[7].coord[1] = 0.0;  vertices[7].coord[2] = 1.0;

	for(int i = 0; i < 6 ;i++) {
		faces[i].nVerts = 4;
		faces[i].v = new Vertex[4];

	}

	faces[0].v[0].vertIndex = 7;  faces[0].v[0].normIndex = 0;
	faces[0].v[1].vertIndex = 7;  faces[0].v[1].normIndex = 0;
	faces[0].v[2].vertIndex = 5;  faces[0].v[2].normIndex = 0;
	faces[0].v[3].vertIndex = 4;  faces[0].v[3].normIndex = 0;

	faces[1].v[0].vertIndex = 1;  faces[1].v[0].normIndex = 1;
	faces[1].v[1].vertIndex = 2;  faces[1].v[1].normIndex = 1;
	faces[1].v[2].vertIndex = 3;  faces[1].v[2].normIndex = 1;
	faces[1].v[3].vertIndex = 0;  faces[1].v[3].normIndex = 1;

	faces[2].v[0].vertIndex = 2;  faces[2].v[0].normIndex = 2;
	faces[2].v[1].vertIndex = 6;  faces[2].v[1].normIndex = 2;
	faces[2].v[2].vertIndex = 7;  faces[2].v[2].normIndex = 2;
	faces[2].v[3].vertIndex = 3;  faces[2].v[3].normIndex = 2;

	faces[3].v[0].vertIndex = 0;  faces[3].v[0].normIndex = 3;
	faces[3].v[1].vertIndex = 4;  faces[3].v[1].normIndex = 3;
	faces[3].v[2].vertIndex = 5;  faces[3].v[2].normIndex = 3;
	faces[3].v[3].vertIndex = 1;  faces[3].v[3].normIndex = 3;

	faces[4].v[0].vertIndex = 0;  faces[4].v[0].normIndex = 4;
	faces[4].v[1].vertIndex = 3;  faces[4].v[1].normIndex = 4;
	faces[4].v[2].vertIndex = 7;  faces[4].v[2].normIndex = 4;
	faces[4].v[3].vertIndex = 4;  faces[4].v[3].normIndex = 4;

	faces[5].v[0].vertIndex = 5;  faces[5].v[0].normIndex = 5;
	faces[5].v[1].vertIndex = 6;  faces[5].v[1].normIndex = 5;
	faces[5].v[2].vertIndex = 2;  faces[5].v[2].normIndex = 5;
	faces[5].v[3].vertIndex = 1;  faces[5].v[3].normIndex = 5;

	for(int i = 0; i < 6 ;i++) { newell(i);  /*operator<<(cout, normals[i]); cout << endl; */}


}


Cube::~Cube() {
	delete[] vertices;
	for (int i = 0; i < 6; i++) delete [] faces[i].v;
	delete[] faces;
	delete normals;

}

Icosahedron::Icosahedron() {
	numVertices = 12;
	vertices = new Point3D[numVertices];

	numNorms = 20;
	normals = new Point3D[numNorms];

	numFaces = 20;
	faces = new Face[numFaces];
	for (int i = 0 ; i < numFaces; i++) {
		faces[i].nVerts = 3;
		faces[i].v = new Vertex[3];
	}



	vertices[0].coord[0] = 0.0; vertices[0].coord[1] = 1.0; vertices[0].coord[2] = tau;
	vertices[1].coord[0] = 0.0; vertices[1].coord[1] = 1.0; vertices[1].coord[2] = -tau;
	vertices[2].coord[0] = 1.0; vertices[2].coord[1] = tau; vertices[2].coord[2] = 0.0;
	vertices[3].coord[0] = 1.0; vertices[3].coord[1] = -tau; vertices[3].coord[2] = 0.0;

	vertices[4].coord[0] = 0.0; vertices[4].coord[1] = -1.0; vertices[4].coord[2] = -tau;
	vertices[5].coord[0] = 0.0; vertices[5].coord[1] = -1.0; vertices[5].coord[2] = tau;
	vertices[6].coord[0] = tau; vertices[6].coord[1] = 0.0;  vertices[6].coord[2] = 1.0;
	vertices[7].coord[0] = -tau; vertices[7].coord[1] = 0.0; vertices[7].coord[2] = 1.0;

	vertices[8].coord[0] = tau; vertices[8].coord[1] = 0.0; vertices[8].coord[2] = -1.0;
	vertices[9].coord[0] = -tau; vertices[9].coord[1] = 0.0; vertices[9].coord[2] = -1.0;
	vertices[10].coord[0] = -1.0; vertices[10].coord[1] = tau; vertices[10].coord[2] = 0.0;
	vertices[11].coord[0] = -1.0; vertices[11].coord[1] = -tau; vertices[11].coord[2] = 0.0;

	for(int i  = 0 ; i < 20; i++) {
		for (int j = 0;j < 3 ; j++)
			faces[i].v[j].normIndex = i;
	}

	faces[0].v[0].vertIndex = 0; faces[0].v[1].vertIndex = 6;  faces[0].v[2].vertIndex = 2;
	faces[1].v[0].vertIndex = 6; faces[1].v[1].vertIndex = 3;  faces[1].v[2].vertIndex = 2;
	faces[2].v[0].vertIndex = 6; faces[2].v[1].vertIndex = 5;  faces[2].v[2].vertIndex = 3;
	faces[3].v[0].vertIndex = 7; faces[3].v[1].vertIndex = 5;  faces[3].v[2].vertIndex = 6;
	faces[4].v[0].vertIndex = 7; faces[4].v[1].vertIndex = 6;  faces[4].v[2].vertIndex = 0;

	faces[5].v[0].vertIndex = 2; faces[5].v[1].vertIndex = 3;  faces[5].v[2].vertIndex = 8;
	faces[6].v[0].vertIndex = 1; faces[6].v[1].vertIndex = 2;  faces[6].v[2].vertIndex = 8;
	faces[7].v[0].vertIndex = 0; faces[7].v[1].vertIndex = 2;  faces[7].v[2].vertIndex = 1;
	faces[8].v[0].vertIndex = 10; faces[8].v[1].vertIndex = 0;  faces[8].v[2].vertIndex = 1;
	faces[9].v[0].vertIndex = 10; faces[9].v[1].vertIndex = 1;  faces[9].v[2].vertIndex = 9;

	faces[10].v[0].vertIndex = 1; faces[10].v[1].vertIndex = 8;  faces[10].v[2].vertIndex = 9;
	faces[11].v[0].vertIndex = 3; faces[11].v[1].vertIndex = 4;  faces[11].v[2].vertIndex = 8;
	faces[12].v[0].vertIndex = 5; faces[12].v[1].vertIndex = 4;  faces[12].v[2].vertIndex = 3;
	faces[13].v[0].vertIndex = 11; faces[13].v[1].vertIndex = 4;  faces[13].v[2].vertIndex = 5;
	faces[14].v[0].vertIndex = 11; faces[14].v[1].vertIndex = 7;  faces[14].v[2].vertIndex = 10;

	faces[15].v[0].vertIndex = 7; faces[15].v[1].vertIndex = 0;  faces[15].v[2].vertIndex = 10;
	faces[16].v[0].vertIndex = 9; faces[16].v[1].vertIndex = 4;  faces[16].v[2].vertIndex = 11;
	faces[17].v[0].vertIndex = 9; faces[17].v[1].vertIndex = 8;  faces[17].v[2].vertIndex = 4;
	faces[18].v[0].vertIndex = 11; faces[18].v[1].vertIndex = 5;  faces[18].v[2].vertIndex = 7;
	faces[19].v[0].vertIndex = 11; faces[19].v[1].vertIndex = 10;  faces[19].v[2].vertIndex = 9;

	for (int i = 0 ; i < 20 ; i++) newell(i);
 }


Icosahedron::~Icosahedron() {
	for (int i = 0 ; i < numFaces; i++) delete [] faces[i].v;
	delete[] faces;
	delete[] vertices;
	delete[] normals;

}


BuckyBall::BuckyBall() {
	i = new Icosahedron();
	makeVertices();
	makeFaces();
	numNorms = 32;
	normals = new Point3D[32];
	for(int i = 0; i < 32; i++ ) newell(i);
}

BuckyBall::~BuckyBall() {
	for(int i  = 0; i < 32;i++) delete[] faces[i].v;
	delete[] faces; delete[] normals; delete[] vertices;

}

const int BuckyBall::vdata[] = {9, 4, 9, 11, 9, 10, 9, 1, 10, 1,
				10, 9, 10, 11, 10, 7, 10, 0, 11, 4,   // 9
				11, 5, 11, 7, 0, 2, 0, 6, 0, 7,
				11, 9, 0, 10, 0, 1, 2, 0, 2, 1,        // 19
				2, 8, 2, 3, 2, 6, 1, 9, 1, 10,
				1, 0, 1, 2, 1, 8, 3, 8, 3, 2,         // 29
				3, 6, 3, 5, 3, 4, 4, 5, 4, 11,
				11, 10, 4, 9, 4, 8, 4, 3, 5, 4,         // 39
    				5, 3, 5, 6, 5, 7, 5, 11, 6, 5,
				6, 3, 6, 2, 6, 0, 6, 7, 7, 6,         // 49
				7, 0, 7, 10, 7, 11, 7, 5, 8, 4,
				8, 9, 8, 1, 8, 2, 8, 3, 9, 8 };      // 59

const int BuckyBall::pentData[] = {17, 16, 14, 13, 12,
				   23, 24, 25, 26, 27,
				   18, 22, 21, 20, 19,
				   28, 29, 30, 31, 31,
				   33, 34, 36, 37, 38,
				   39, 40, 41, 42, 43,
				   44, 45, 46, 47, 48,
				   49, 50, 51, 52, 53,
				   54, 55, 56, 57, 58,
				   59, 0, 1, 2, 3,
				   4, 5, 6, 7, 8,
				   9, 10, 11, 35, 15 };

const int BuckyBall::hexData[] =
	{ 22, 18, 12, 13, 47, 46,
	  29, 21, 22, 46, 45, 30,
	  30, 45, 44, 41, 40, 31,
	  44, 48, 49, 53, 42, 41,
	  47, 13, 14, 50, 49, 48,
	  58, 57, 20, 21, 29, 28,
	  57, 56, 27, 26, 19, 20,
	  19, 26, 25, 17, 12, 18,
	  25, 24, 4, 8, 16, 17,
	  23, 3, 2, 5, 4, 24,       // 9

	  55, 59, 3, 23, 27, 56,
	  37, 54, 58, 28, 32, 38,
	  38, 32, 31, 40, 39, 33,
	  43, 10, 9, 34, 33, 39,
	  6, 35, 11, 52, 51, 7,
	  16, 8, 7, 51, 50, 14,
	  34, 9, 15, 1, 0, 36,
	  36, 0, 59, 55, 54, 37,
	  42, 53, 52, 11, 10, 43,
	  1, 15, 35, 6, 5, 2  };


#define THIRD 0.333333333
#define TWO_THIRDS 0.666666666

void
BuckyBall::makeVertices() {
	numVertices = 60;
	vertices = new Point3D[60];
	Point3D temp1, temp2;

	for(int k = 0; k < 60; k++ ) {
		mult(TWO_THIRDS, &(i->vertices[vdata[2*k]]), &temp1);
		mult(THIRD, &(i->vertices[vdata[2*k+1]]), &temp2);
		add(temp1, temp2, vertices[k]);
	}

}

void
BuckyBall::makeFaces() {
	numFaces = 32;
	faces = new Face[32]; //first 12 are pentagons, one for each point in icos.
	for(int i  = 0; i < 12; i++) {
		faces[i].nVerts = 5;
		faces[i].v = new Vertex[5];
		for(int j = 0; j < 5; j++) {
			faces[i].v[j].normIndex = i;
			faces[i].v[j].vertIndex = pentData[5*i + j];
		}
	}

	for(int i = 0; i < 20; i++) {
		faces[i+12].nVerts = 6;
		faces[i+12].v = new Vertex[6];
		for(int j = 0; j < 6; j++) {
			faces[i+12].v[j].normIndex = i+12;
			faces[i+12].v[j].vertIndex = hexData[6*i + j];
		}
	}

}

// this sphere is always centered at the origin and has a unit radius
Sphere::Sphere(int resolution) : n(resolution) {

	numFaces = totalFaces();
	faces = new Face[numFaces];

	numVertices = totalVertices();
	vertices = new Point3D[numVertices]; /* the poles are the last two vertices */

	numNorms = numVertices;
	normals = new Point3D[numNorms];
	numCircles = (n-2)/2;

	makeVertices();
	makeFaces();
	makeNormals();
	
}

Sphere::~Sphere() {
	for(int i = 0; i < numFaces; i++)
		delete [] faces[i].v;
				
	delete [] faces; delete [] vertices; delete [] normals;
}


void Sphere::makeVertices() {

	/* north pole */
	vertices[numVertices - 2].setCoords(0.0f, 0.0f, 1.0f);

	/* south pole */
	vertices[numVertices - 1].setCoords(0.0f, 0.0f, -1.0f);

	/* make all the horizontal circles */
	
	const float alpha = 2 * M_PI / n;
	float alpha_m = alpha;
	
	for(int m = 0; m < numCircles; m++) {
	
		makeCircle(sin(alpha_m), cos(alpha_m), vertices+m*n);
		alpha_m += alpha;
	}
	
}


void Sphere::makeFaces() {
	
	/* make north pole tri's */
	for(int i = 0; i < n; i++) {
		faces[i].nVerts = 3;
		faces[i].v = new Vertex[3];

		faces[i].v[0].vertIndex = numVertices - 2; //northpole
		faces[i].v[0].normIndex = numVertices - 2;

		faces[i].v[1].vertIndex = (i + 1) % n;
		faces[i].v[1].normIndex = (i + 1) % n;

		faces[i].v[2].vertIndex = i;
		faces[i].v[2].normIndex = i;	
	}

	/* make south pole tri's*/
	const int offset = numVertices - 2 - n;

	for(int i = 0 ;i < n; i++) {
		faces[n+i].nVerts = 3;
		faces[n+i].v = new Vertex[3];

		faces[n+i].v[0].vertIndex = numVertices - 1; // southpole
		faces[n+i].v[0].normIndex = numVertices - 1;

		faces[n+i].v[1].vertIndex = offset + ( (i + 1) % n );
		faces[n+i].v[1].normIndex = offset + ( (i + 1) % n );

		faces[n+i].v[2].vertIndex = offset + i;
		faces[n+i].v[2].normIndex = offset + i;
	}	
	
	/* make all the other quads */
	for(int i = 0; i < numCircles - 1; i++) {	// for each "ring" of quads
		
			int top_os = (i)*n;				// vertex offset for the top circle for this ring
			int low_os = (i+1)*n;				// vertex offset for the lower circle for this ring 
			int face_os = (2+i)*n;				// face offset for this ring
			
		for(int j = 0; j < n; j++) {			// for each quad in the ring
			
			int fi = face_os + j;
			faces[fi].nVerts = 4;
			faces[fi].v = new Vertex[4];
			
			faces[fi].v[0].vertIndex = faces[fi].v[0].normIndex = top_os + j;
			faces[fi].v[1].vertIndex = faces[fi].v[1].normIndex = top_os + (j+1)%n;
			faces[fi].v[2].vertIndex = faces[fi].v[2].normIndex = low_os + (j+1)%n;
			faces[fi].v[3].vertIndex = faces[fi].v[3].normIndex = low_os + j;
		}
	}
		
}


void Sphere::makeCircle(float radius, float z, Point3D* result) {

	const int reps = n / 4;
	const int n_on_2 = n / 2;
	const float alpha = 2 * M_PI / n;
	float alpha_m = 0;
	float delta_x, delta_y;
	
	for(int m = 0; m <= reps; m++) {

		delta_x = radius * sin(alpha_m);	
		delta_y = radius * cos(alpha_m);
		
		result[m].setCoords(delta_x, delta_y, z);
		result[n_on_2 - m].setCoords(delta_x, - delta_y, z);
		result[n_on_2 + m].setCoords(- delta_x, - delta_y, z);
		result[(n - m)%n].setCoords(-delta_x, delta_y, z);

		alpha_m += alpha;
	}
}

void Sphere::makeNormals() {

	for(int i = 0; i < numNorms; i++) {
		normals[i] = vertices[i];
		normalise(&normals[i]); // neccessary??
	}
}

int Sphere::totalVertices() {

	return ( n * (n - 2) / 2 ) + 2;
}

int Sphere::totalFaces() {

	return 2*n + n * ( (n - 2) / 2 - 1);
}


StripedSphere::StripedSphere(int resolution) :
	Sphere(resolution), listIndex(20) {
	glNewList(listIndex, GL_COMPILE);
		drawCommands();
	glEndList();
}

void
StripedSphere::drawCommands() {

	GLfloat red[] = {1.0f, 0.0f, 0.0f, 1.0f};
	GLfloat white[] = {0.7f, 0.7f, 0.7f, 1.0f};
	GLfloat* col;
	int fi = 0;			// face index
	for(int k = 0; k < numFaces / n; k++) {	
		
		if(k & 1) col = red; else col = white;
		glMaterialfv(GL_FRONT, GL_DIFFUSE, col);
		glMaterialfv(GL_FRONT, GL_AMBIENT, col);
		glMaterialfv(GL_FRONT, GL_SPECULAR, col);
		
		for(int i = 0; i < n ;i++, fi++) {

			glBegin(GL_POLYGON);
			for(int j = 0; j < faces[fi].nVerts; j++) {
				int iv  = faces[fi].v[j].vertIndex;
				int in  = faces[fi].v[j].normIndex;
				glNormal3f(normals[in].coord[0], normals[in].coord[1],
					normals[in].coord[2]);
				glVertex3f(vertices[iv].coord[0], vertices[iv].coord[1], vertices[iv].coord[2]);
			}
			glEnd();
		}
	}

}


void
StripedSphere::draw() const {
	glCallList(listIndex);
	//drawCommands();
}
