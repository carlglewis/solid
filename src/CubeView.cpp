
#include "CubeView.h"



CubeView::CubeView(QWidget * parent, const char * name):
	QGLWidget(parent,name){

	count = 0; solid = 2;
	time = new QTimer(this, "timer");
	connect(time, SIGNAL(timeout()), this, SLOT(updateGL()));
	time->start(40);
}



void CubeView::initializeGL() {
	glClearColor (0.0, 0.0, 0.0, 0.0);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);


	GLfloat lightIntensity[] = {1.0f, 0.2f, 0.2f, 1.0f};
	GLfloat lightPosition[] = {3.0f, -6.0f, 3.0f, 2.0f};
	GLfloat lightIntenSpec[] = {0.0f, 0.4f, 0.4f, 1.0f};
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightIntensity);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightIntenSpec);

	GLfloat lightIntensity1[] = {0.1f, 0.1f, 0.9f, 1.0f};
	GLfloat lightPosition1[] = {-3.0f, 3.0f, 3.0f, 2.0f};
	glLightfv(GL_LIGHT1, GL_POSITION, lightPosition1);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightIntensity1);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lightIntensity1);

	glEnable(GL_CULL_FACE);

	defaultMaterials();
	
	lc = new LightControl(this, "light control");
	lc->show();

	figures[0] = new Cube();
	figures[1] = new Icosahedron();
	figures[2] = new BuckyBall();
	figures[3] = new StripedSphere(36);

}



void CubeView::paintGL() {
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix ();
		//glRotated(( (count-=8) %360), 0, 1, 0);
		//glTranslatef(0.0, 0.0, -4.5);
		glRotated(( (count+=2) %360), 0, 1, 0);
		if(solid == 0) glTranslatef(-0.5, -0.5, -0.5);
		figures[solid]->draw();
	glPopMatrix ();

}

void CubeView::resizeGL ( int w, int h ) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0, 1.0*(GLfloat)w/(GLfloat)h, 1.0, 30.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -3.0);
	glRotatef(25.0, 1.0, 0.0, 0.0);
}

void
CubeView::keyReleaseEvent( QKeyEvent * ke) {

	if(ke->key() == Qt::Key_S) {
		//QPixmap pm = renderPixmap();
		QPixmap pm = QPixmap::grabWindow(this->winId());
		pm.save("sshot.bmp", "BMP");
	}
	else {
		solid = (solid+1) %4;
		defaultMaterials();
	}

}

void 
CubeView::defaultMaterials() {

	GLfloat mat_ambient[] = {0.7f, 0.7f, 0.7f, 1.0f};
	GLfloat mat_diffuse[] = {0.6f, 0.6f, 0.6f, 1.0f};
	GLfloat mat_specular[] = {0.5f, 0.5f, 0.5f, 1.0f};
	GLfloat mat_shininess[] = {50.0f};

	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
}


int main(int argc, char** argv)
{
	QApplication app(argc, argv );
	CubeView *t = new CubeView();
	app.setMainWidget(t);
	t->show();
	return app.exec();
}
