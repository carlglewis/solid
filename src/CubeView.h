#include <GL/gl.h>
#include <GL/glu.h>
#include <qgl.h>
#include <qapplication.h>
#include <qtimer.h>
#include <iostream>
#include <cstdlib>
#include <qpixmap.h>

#include "Point3D.h"
#include "Mesh.h"
#include "LightControl.h"


#ifndef CL_CUBEVIEW_H
#define CL_CUBEVIEW_H



class CubeView : public QGLWidget {

public:
	CubeView(QWidget * parent=0, const char * name=0);

protected:
 	virtual void initializeGL ();
	virtual void resizeGL ( int w, int h );
	virtual void paintGL ();
	virtual void keyReleaseEvent( QKeyEvent * );

	void defaultMaterials();

private:
	int count;
	QTimer* time;
	Mesh* figures[4];
	int solid;
	LightControl* lc;
} ;

#endif




