#ifndef CL_POINT_H
#define CL_POINT_H
#include <math.h>
#include <iostream>
struct Point3D {
	float coord[3];
	Point3D(float x, float y, float y);
	Point3D(){}
	Point3D& operator=(const Point3D&);
	operator float*() { return coord; }
	operator const float*()const {return coord; }
	float & operator[] (int i) { return coord[i]; }
	float operator[] (int i) const { return coord[i]; }
	void setCoords(float x, float y, float z);
	Point3D & operator+=(const Point3D&);
};

float dotProduct(Point3D* a, Point3D* b);

float dotProduct(Point3D* a, float* b);

void crossProduct(float* a, float* b, float* result);

float length(Point3D* a);

// reduce argument to unit length
void normalise(Point3D* a);

// result = a - b
void sub(const float* a, const float* b, float* result) ;

void add(const float* a, const float* b, float* result);

void mult(float scalar, Point3D* a);

void mult(float scalar, Point3D* in, Point3D* out);

std::ostream& operator<<(std::ostream&, const Point3D &);

inline std::istream& operator>>(std::istream& is, Point3D& p){

	is >> p.coord[0]; is >> p.coord[1]; is >> p.coord[2];
	return is;
}

inline void Point3D::setCoords(float x, float y, float z) {
	
	coord[0] = x; coord[1] = y; coord[2] = z;
}


#endif



