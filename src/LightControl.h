

#ifndef CL_LIGHT_CONTROL_H
#define CL_LIGHT_CONTROL_H

#include <qhbox.h>
#include <qvbox.h>
#include <qslider.h>
#include <qcombobox.h>
#include <qlabel.h>
#include <qstring.h>
#include <qcheckbox.h>
#include <GL/gl.h>
#include <qapplication.h>
#include <qlayout.h>
#include <qdialog.h>


class SuperSlider : public QWidget {

	Q_OBJECT

	QLabel* name;
	QLabel* readout;
	QHBoxLayout* layout;
	QString strVal, wide;
	float maxVal, minVal;
	QSlider* slider;
public:

	SuperSlider(const char* label, QWidget* parent = 0, const char* name = 0);
	void setMaxValue(float newval);
	void setMinValue(float newval);
	void setValue(float val);
	float value();

private:
	void setReadout(float);

private slots:
	void setReadout(int);

signals:
	void sliderMoved(int);
};


class LightControl : public QDialog {

	Q_OBJECT

	QComboBox* pickLight;
	QCheckBox* enabled;
	QCheckBox* spotSwitch;
	QLabel* label;
	QComboBox* parameter;
	SuperSlider* sliders[4];
	QGridLayout* mainLayout;

	void showState();
	GLenum light;
	GLenum param;

	static const GLenum lightList[8];
	static const GLenum paramList[7];

public:
	LightControl(QWidget* parent = 0, const char* name = 0);

public slots:
	void valueChanged(int);   //dummy arguments for both functions
	void selectionChanged(int);
	void lightSwitch(bool);

};



#endif


