#include "LightControl.h"


SuperSlider::SuperSlider(const char* label, QWidget* parent,
	const char* name): QWidget(parent, name) {
	layout = new QHBoxLayout(this, 1, 2);
	this->name  = new QLabel(label, this);
	this->name->setFixedSize(35, 15);
	slider = new QSlider(QSlider::Horizontal, this);
	readout = new QLabel(this);
	readout->setFixedSize(40, 15);
	layout->addWidget(this->name);
	layout->addWidget(slider);
	layout->addWidget(readout);

	connect(slider, SIGNAL(sliderMoved(int)), this, SLOT(setReadout(int)));
	connect(slider, SIGNAL(sliderMoved(int)), this, SIGNAL(sliderMoved(int)));
	// slider->setValue(100);
	// slider->setValue(0);
	maxVal = 1.0; minVal = 0.0;
}

// this accepts the value from QSlider::sliderMoved()
// it assumes that the value of the slider varies from 0 to 99
void
SuperSlider::setReadout(int val) {
	float f = (( (float) val ) * (maxVal - minVal)/ 100.0) + minVal ;
	setReadout(f);
}

void
SuperSlider::setReadout(float val) {
	strVal.setNum(val, 'f', 2);
	wide = strVal.rightJustify(6);
	readout->setText(wide);
}


void
SuperSlider::setMaxValue(float newVal){
	maxVal = newVal;
}


void
SuperSlider::setMinValue(float newVal){
	minVal = newVal;
}


void
SuperSlider::setValue(float val){
	if(val > maxVal) val = maxVal;
	else if(val < minVal) val = minVal;

	int spos = (int) (((val - minVal)/ (maxVal - minVal)) *100.0);
	slider->setValue(spos);
	setReadout(val);
}


float
SuperSlider::value(){
	float sval = (float) slider->value();
	return ((maxVal - minVal) * sval / 100.0) + minVal;

}


const GLenum LightControl::lightList[8] = {GL_LIGHT0, GL_LIGHT1, GL_LIGHT2, GL_LIGHT3,
					   GL_LIGHT4, GL_LIGHT5, GL_LIGHT6, GL_LIGHT7};

const GLenum LightControl::paramList[7] = {GL_AMBIENT, GL_DIFFUSE, GL_SPECULAR,
			GL_POSITION, GL_SPOT_DIRECTION, GL_SPOT_EXPONENT, GL_SPOT_CUTOFF};



LightControl::LightControl(QWidget* parent, const char* name):
	QDialog(parent, name){

	setCaption("light control");
	mainLayout = new QGridLayout(this, 1, 1, 1, 2);

	label = new QLabel("Light", this, "lbl: Light");
	pickLight = new QComboBox(false, this, "pickLight");
	pickLight->insertItem("LIGHT0"); pickLight->insertItem("LIGHT1");
	pickLight->insertItem("LIGHT2"); pickLight->insertItem("LIGHT3");
	pickLight->insertItem("LIGHT4"); pickLight->insertItem("LIGHT5");
	pickLight->insertItem("LIGHT6"); pickLight->insertItem("LIGHT7");
	light = GL_LIGHT0;

	enabled = new QCheckBox("enable ", this, "enable");
	spotSwitch = new  QCheckBox("spot", this, "spot");
	parameter = new QComboBox(false, this, "parameter");

	parameter->insertItem("Ambient");
	parameter->insertItem("Diffuse");
	parameter->insertItem("Specular");
	parameter->insertItem("Position");
	parameter->insertItem("Spot Direction");
	parameter->insertItem("Spot Exponent");
	parameter->insertItem("Spot Cutoff");
	parameter->setCurrentItem(1);
	param = GL_DIFFUSE;

	mainLayout->addWidget(label, 0, 0) ;
	mainLayout->addWidget(pickLight, 1, 0);
       	mainLayout->addWidget(enabled, 0, 1);
	mainLayout->addWidget(parameter, 1, 1);


	sliders[0] = new SuperSlider(" R | x ", this);
	mainLayout->addMultiCellWidget(sliders[0], 2, 2, 0, 1);
	sliders[1] = new SuperSlider(" G | y ", this);
	mainLayout->addMultiCellWidget(sliders[1], 3, 3, 0, 1);
	sliders[2] = new SuperSlider(" B | z ", this);
	mainLayout->addMultiCellWidget(sliders[2], 4, 4, 0, 1);
	sliders[3] = new SuperSlider(" A | w ", this);
	mainLayout->addMultiCellWidget(sliders[3], 5, 5, 0, 1);

	mainLayout->addWidget(spotSwitch, 6, 0);

	connect(pickLight, SIGNAL(activated(int)), this, SLOT(selectionChanged(int)));
	connect(parameter, SIGNAL(activated(int)), this, SLOT(selectionChanged(int)));
	showState();
	for(int i = 0; i < 4; i++)
		connect(sliders[i], SIGNAL(sliderMoved(int)),
			this, SLOT(valueChanged(int)));

	connect(enabled, SIGNAL(toggled(bool)), this, SLOT(lightSwitch(bool)));
}


// this displays the current values of the current
// light and parameter on the sliders.
void
LightControl::showState(){
	enabled->setChecked(glIsEnabled(light) == GL_TRUE );
	if(param == GL_AMBIENT || param == GL_DIFFUSE
		|| param == GL_SPECULAR ) {
		float values[4];
		glGetLightfv(light, param, values);
		for(int i  = 0; i < 4; i++) {
			sliders[i]->setMaxValue(1.0);
			sliders[i]->setMinValue(0.0);
			sliders[i]->setValue(values[i]);
		}
	}
	else if(param == GL_POSITION) {
		float values[4];
		glGetLightfv(light, param, values);
		for(int i  = 0; i < 4; i++) {
			sliders[i]->setMaxValue(20.0);
			sliders[i]->setMinValue(-20.0);
			sliders[i]->setValue(values[i]);
		}
	}
	else {
		for(int i  = 0; i < 4; i++)  {
			sliders[i]->setMaxValue(1.0);
			sliders[i]->setMinValue(0.0);
			sliders[i]->setValue(0.0);
		}

	}
}



void
LightControl::valueChanged(int ){
	if(!enabled->isChecked()) return;
	if(param == GL_AMBIENT || param == GL_DIFFUSE || param == GL_SPECULAR ) {
		float values[4];
		for(int i = 0; i < 4;i++)
			values[i] = sliders[i]->value();
		glLightfv(light, param, values);
	}
	else if(param == GL_POSITION) {
		float values[4];
		for(int i = 0; i < 4;i++)
			values[i] = sliders[i]->value();
		glLightfv(light, param, values);

	}
}


void
LightControl::selectionChanged(int i){
	light = lightList[pickLight->currentItem()];
	param = paramList[parameter->currentItem()];
	showState();
}


void
LightControl::lightSwitch(bool on){
	if(on) glEnable(light);
	else glDisable(light);
	showState();
}
/*
int main(int argc, char** argv)
{
	QApplication app(argc, argv );
	LightControl *t = new LightControl ();
	app.setMainWidget(t);
	t->show();
	return app.exec();
}  */


